﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.IO;
using Microsoft.Win32;

namespace TextEditor
{
    public partial class MainWindow : Window
    {
        private List<string> list = new List<string>();
        private tChange t = new tChange();
        public MainWindow()
        {
            InitializeComponent();
            selectSize.ItemsSource = new List<double>() { 8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 72 };
            selectSize.SelectedIndex = 5;
            t = new tChange(textArea);
            FontFamily font = new FontFamily("TimesNewRoman");
            t.setFont = font;
            t.setSize = 14;
            listLoad();
        }
        private void listLoad()
        {
            StreamReader objReader = new StreamReader("syntax.txt");
            string sLine = "";
            while (sLine != null)
            {
                sLine = objReader.ReadLine();
                if (sLine != null)
                    list.Add(sLine);
            }
            objReader.Close();
        }
        private void loader(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            foreach (FontFamily fontFamily in Fonts.SystemFontFamilies)
            {
                data.Add(fontFamily.Source);
            }
            selectFont.ItemsSource = data;
            selectFont.SelectedIndex = 61;
        }
        private void formatText(object sender, ExecutedRoutedEventArgs arg)
        {
            string name = (arg.Command as RoutedUICommand).Name;
            switch (name)
            {
                case "ToggleBold":
                    EditingCommands.ToggleBold.Execute(null, textArea);
                    break;
                case "ToggleItalic":
                    EditingCommands.ToggleItalic.Execute(null, textArea);
                    break;
                case "ToggleUnderline":
                    EditingCommands.ToggleUnderline.Execute(null, textArea);
                    break;
            }
        }
        private void textAreaChanged(object sender, RoutedEventArgs e)
        {
            t.changeFontStyle(ButBold, ButItalic, ButUnderline, selectFont, selectSize);
        }
        private void fontChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            switch (box.Name)
            {
                case "selectFont":
                    t.changeFontFam(selectFont,TextElement.FontFamilyProperty);
                    break;
                case "selectSize":
                    t.changeFontFam(selectSize,TextElement.FontSizeProperty);
                    break;
            }
        }

        private void textArea_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (ButSyntax.IsChecked == true)
                t.syntaxHiglighting(list);
            if (textArea.Selection.IsEmpty)
            {
                if (ButBold.IsChecked == true) 
                    textArea.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, new SolidColorBrush(Colors.Black));
                else
                {
                    textArea.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, new SolidColorBrush(Colors.Black));
                    textArea.Selection.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Normal);
                }
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Process proc;
            proc = Process.GetCurrentProcess();
            proc.Kill();
        }

        private void open()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Rich Text Format (*.rtf)|*.rtf|All files (*.*)|*.*";
            if (dlg.ShowDialog() == true)
            {
                FileStream fileStream = new FileStream(dlg.FileName, FileMode.Open);
                TextRange range = new TextRange(textArea.Document.ContentStart, textArea.Document.ContentEnd);
                range.Load(fileStream, DataFormats.Rtf);
            }
        }

        private void save()
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "Rich Text Format (*.rtf)|*.rtf|All files (*.*)|*.*";
            if (dlg.ShowDialog() == true)
            {
                FileStream fileStream = new FileStream(dlg.FileName, FileMode.Create);
                TextRange range = new TextRange(textArea.Document.ContentStart, textArea.Document.ContentEnd);
                range.Save(fileStream, DataFormats.Rtf);
            }
        }
        private void butClick(object sender,RoutedEventArgs arg)
        {
            Button button = (Button)sender;
            switch (button.Name)
            {
                case "openBut":
                    open();
                    break;
                case "saveBut":
                    save();
                    break;
                case "WindowMinimize":
                    this.WindowState = WindowState.Minimized;
                    break;
                case "WindowRestore":
                    if (this.ActualWidth != this.MaxWidth)
                        this.WindowState = WindowState.Maximized;
                    else
                        this.WindowState = WindowState.Normal;
                    break;
                case "WindowClose":
                    this.Close();
                    break;
                default:
                    break;
            }
        }
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
    }
}
