﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.IO;
using System.Text.RegularExpressions;

namespace TextEditor
{
    class tChange:Window
    {
        private RichTextBox container;
        public tChange()
        {
            container = new RichTextBox();
        }
        public tChange(RichTextBox box)
        {
            container = box;
        }
        public FontFamily setFont
        {
            set
            {
                container.FontFamily = value;
            }
        }
        public double setSize
        {
            set
            {
                container.FontSize = value;
            }
        }
        public void changeFontFam(ComboBox box, DependencyProperty e)
        {
            if (box.SelectedItem != null)
                this.container.Selection.ApplyPropertyValue(e, box.SelectedItem);
        }
        public void syntaxHiglighting(List<string> list)
        {
            string el = list[0];
            string[] word = el.Split(' ','\t');
            TextRange range = new TextRange(container.Document.ContentStart, container.Document.ContentEnd);
            for (int i = 0; i < word.Length; i++)
            {
                Regex reg = new Regex(word[i], RegexOptions.Compiled | RegexOptions.IgnoreCase);
                var start = container.Document.ContentStart;
                while (start != null && start.CompareTo(container.Document.ContentEnd) < 0)
                {
                    if (start.GetPointerContext(LogicalDirection.Forward) == TextPointerContext.Text)
                    {
                        var match = reg.Match(start.GetTextInRun(LogicalDirection.Forward));
                        var textrange = new TextRange(start.GetPositionAtOffset(match.Index, LogicalDirection.Forward), start.GetPositionAtOffset(match.Index + match.Length, LogicalDirection.Backward));
                        textrange.ApplyPropertyValue(TextElement.ForegroundProperty, new SolidColorBrush(Colors.Blue));
                        textrange.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Black);
                        start = textrange.End;
                    }
                    start = start.GetNextContextPosition(LogicalDirection.Forward);
                }
            }
        }
        public void changeFontStyle(ToggleButton bold, ToggleButton italic, ToggleButton underline, ComboBox box1, ComboBox box2)
        {
            object obj = container.Selection.GetPropertyValue(TextElement.FontWeightProperty);
            bold.IsChecked = (obj != DependencyProperty.UnsetValue) && (obj.Equals(FontWeights.Bold));
            obj = container.Selection.GetPropertyValue(TextElement.FontStyleProperty);
            italic.IsChecked = (obj != DependencyProperty.UnsetValue) && (obj.Equals(FontStyles.Italic));
            obj = container.Selection.GetPropertyValue(Inline.TextDecorationsProperty);
            underline.IsChecked = (obj != DependencyProperty.UnsetValue) && (obj.Equals(TextDecorations.Underline));
            if (container.Selection.IsEmpty)
            {
                container.Selection.ApplyPropertyValue(TextElement.FontFamilyProperty, box1.SelectedItem);
                container.Selection.ApplyPropertyValue(TextElement.FontSizeProperty, box2.SelectedItem);
            }
            else
            {
                obj = container.Selection.GetPropertyValue(TextElement.FontFamilyProperty);
                box1.SelectedItem = obj;
                obj = container.Selection.GetPropertyValue(TextElement.FontSizeProperty);
                box2.SelectedItem = obj;
            }
        }
    }
}
